<?php

namespace App\Http\Controllers;
use App\Models\almacenes;
use Illuminate\Http\Request;

class almacenesController extends Controller
{
    public function lista_almacenes(){
        $almacenes = almacenes::all();

        return $almacenes;
    }

    public function guarda_almacenes(Request $request){
        if($request->id == 0){
            $almacenes = new almacenes();
        }else{
            $almacenes = almacenes::find($request->id);
        }
        $almacenes->nomalmacen = $request->nomalmacen;
        $almacenes->telefono = $request->telefono;
        $almacenes->direccion = $request->direccion;
        $almacenes->ciudad = $request->ciudad;
  
        $almacenes->estado = $request->estado;
        $almacenes->save();

        return response()->json($almacenes,200);
    }
    public function borrar_almacenes(Request $request){
        $almacenes = almacenes::find($request->id);

        $almacenes->delete();
        return response()->json("Eliminado",200);
    }
    public function listar_almacenes_filtro(Request $request){
        $almacenes = almacenes::where('nomalmacen', 'like', $request->nomalmacen)->get();
        return $almacenes;
    }
}
